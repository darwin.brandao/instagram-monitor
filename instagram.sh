#!/bin/bash

[ $( command -v notify-send ) ] && notify-send "Garoa98" "Sending daily report" || echo "Garoa98: Sending daily report"

if ! [ $(command -v jq) ]; then
        echo "Error: 'jq' not installed."
        exit 1
fi

# Directory of the script, not the working directory
# DIR=$( cd -- "$( dirname -- "$(readlink ${BASH_SOURCE[0]})" )" &> /dev/null && pwd )
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

source "${DIR}/config/bot.cfg"

debug=$( cat "${DIR}/config/debug.cfg" )

# Make snapshots directory
[ ! -d "${DIR}/snapshots/" ] && mkdir "${DIR}/snapshots/"

interval=3
feed=""
count=0
while [ $count -lt 10 ]; do

	echo "Fetching data ($( date +'%d-%m-%Y %H:%M:%S' ))"

	# Fetch data from Instagram account
	filename="${DIR}/snapshots/$( date +'%d-%m-%Y' ).json"
	[ ! -f "$filename" ] \
		&& feed=`curl -s --user-agent "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0" "https://www.instagram.com/garoa98/?__a=1&__d=dis"` \
		|| feed=`cat "${DIR}/snapshots/$( date +'%d-%m-%Y' ).json"`

	# Check if request returned data
	[ "$( echo "$feed" | jq .status )" == "\"fail\"" ] && echo "Failed... Trying again." || break

	(( count++ ))

	if [ $count -eq 10 ]; then

		new_interval=$(( $interval * 2 ))

		# curl --data-urlencode "text=$( echo -e "O bot não obteve resposta do Instagram em 10 tentativas consecutivas (1 a cada ${interval} minutos).\nAumentando o intervalo de tentativa de ${interval} para ${new_interval} minutos" )" "https://api.telegram.org/bot${api_token}/sendMessage?chat_id=${chat_id}&parse_mode=markdown"
		
		notify-send "Garoa98" "O bot não obteve resposta do Instagram em 10 tentativas consecutivas (1 a cada ${interval} minutos).\nAumentando o intervalo de tentativa de ${interval} para ${new_interval} minutos"

		count=0
		interval=$new_interval
	fi

	if [ $interval -gt 60 ]; then

		# curl --data-urlencode "text=$( echo -e "O intervalo de requisição pro Instagram superou 1 hora. Abortando o bot." )" "https://api.telegram.org/bot${api_token}/sendMessage?chat_id=${chat_id}&parse_mode=markdown"
		
		notify-send "Garoa98" "O intervalo de requisição pro Instagram superou 1 hora. Abortando o bot."

		exit 1
	fi

	sleep `echo "60 * $interval" | bc`
done

echo "Instagram feed data received ($( date +'%d-%m-%Y %H:%M:%S' ))"

max_posts=3

last_snapshot=$( ls -Art "${DIR}/snapshots" | tail -n1 )
[ "$last_snapshot" == "$( date +'%d-%m-%Y.json' )" ] && last_snapshot=$( ls -Art "${DIR}/snapshots" | sed '$d' | tail -n1 )

follower_count=`echo "$feed" | jq .graphql.user.edge_followed_by.count`

imgs_like_count=`echo "$feed" | jq .graphql.user.edge_owner_to_timeline_media.edges[].node.edge_liked_by.count | paste -sd+ | bc`
vids_like_count=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[].node.edge_liked_by.count | paste -sd+ | bc`

last_imgs_like_count=`cat "${DIR}/snapshots/${last_snapshot}" | jq .graphql.user.edge_owner_to_timeline_media.edges[].node.edge_liked_by.count | paste -sd+ | bc`
last_vids_like_count=`cat "${DIR}/snapshots/${last_snapshot}" | jq .graphql.user.edge_felix_video_timeline.edges[].node.edge_liked_by.count | paste -sd+ | bc`

last_like_count=`echo $last_imgs_like_count+$last_vids_like_count | bc`
like_count=`echo $vids_like_count+$imgs_like_count | bc`

new_followers=`echo ${follower_count}-$( cat "${DIR}/snapshots/${last_snapshot}" | jq .graphql.user.edge_followed_by.count) | bc`
new_likes=`echo ${like_count}-${last_like_count} | bc`

# Metas
source "${DIR}/config/metas.cfg"
bar_units=20

insta_monthly_goal=`echo "( ${insta_followers_target} - ${insta_followers_init} ) / 12 * $( date +%m )" | bc`
insta_followers_curr_month=`echo "${follower_count} - ${insta_followers_init}" | bc`
insta_progress_pctg=`echo "scale=2; 100 * ${insta_followers_curr_month} / ${insta_monthly_goal}" | bc`
insta_progress=`echo "${bar_units} / 100 * ${insta_progress_pctg}" | bc`

# Build message
msg="📸 *Instagram Garoa98* ($(date +'%d/%m/%Y'))

📈 *Crescimento* desde o dia $( echo "${last_snapshot%.*}" | tr - / )

   ✅ *Seguidores:* `[ "${new_followers:0:1}" != "-" ] && echo "+${new_followers}" || echo $new_followers`
   ✅ *Likes:* `[ "${new_likes:0:1}" != "-" ] && echo "+${new_likes}" || echo $new_likes`

 📋 Status

   👫 *Seguidores:* ${follower_count}
   ❤️  *Likes:* ${like_count}
          - *Videos:* ${vids_like_count}
          - *Feed:* ${imgs_like_count}

 🎯 Metas:

   🟢 *Instagram:*
   🔹 *Seguidores:* 
       ${insta_followers_curr_month} \[$( "${DIR}/submodules/progress-bar/progress-bar.sh" ${insta_followers_curr_month} ${insta_monthly_goal} )] ${insta_monthly_goal} \[${insta_progress_pctg}%] (${insta_followers_curr_month}/${insta_monthly_goal})
	  
 📆 Últimos vídeos
"

for ((i = 0 ; i < $max_posts ; i++)); do
	view_count=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[$i].node.video_view_count`
	like_count=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[$i].node.edge_liked_by.count`
	duration=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[$i].node.video_duration`
	finish_time=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[$i].node.taken_at_timestamp`
	url=`echo "$feed" | jq .graphql.user.edge_felix_video_timeline.edges[$i].node.video_url`
	
	[ "$url" == null ] && continue

	msg="${msg}\n 🎥 *Vídeo:* `date -d @${finish_time} +'%d/%m/%Y %H:%M:%S'`"
	msg="${msg}\n    *👁 Views:* ${view_count}"
	msg="${msg}\n    *❤️  Likes:* ${like_count}"
	msg="${msg}\n    *🕘 Duração:* `date -d @${duration} -u +'%Hh %Mm %Ss'`"
	#msg="${msg}\n\tLink Download: ${url}"
	msg="${msg}\n"
done

# Send message
[ $debug = false ] \
	&& curl --data-urlencode "text=$( echo -e "$msg" )" "https://api.telegram.org/bot${api_token}/sendMessage?chat_id=${chat_id}&parse_mode=markdown" \
	|| echo -e "$msg"

# Save snapshot
mkdir -p "${DIR}/snapshots"
echo "$feed" | jq . > "${DIR}/snapshots/$( date +'%d-%m-%Y' ).json"
